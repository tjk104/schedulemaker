import sys
import os
import csv


def check_intersect(class1, class2):
    day_check = any(item in list(class2[3]) for item in list(class1[3]))
    class1_times = [int(x) for x in class1[4].split("-")]
    class2_times = [int(x) for x in class2[4].split("-")]
    time_check = class1_times[0] > class2_times[1] or class2_times[0] > class1_times[1]
    return day_check and not time_check


file_options = os.listdir('../data/')

print('\nWhich file would you like to use?')
for i, path in enumerate(file_options):
    print('\t{}: {}'.format(i, path))

file_index = -1
while file_index not in [str(n) for n in range(len(file_options))]:
    file_index = input('Enter the file number: ')
file_index = int(file_index)

file_name = '../data/' + file_options[file_index]

sys.stdout.write("Parsing classes...")
data_list = []

with open(file_name) as data_file:
    csv_file = csv.reader(data_file)
    for row in csv_file:
        data_list.append(row)

sys.stdout.write(" Done!\n")

sys.stdout.write("Cleaning data...")
# used fields (in order): CRN, course (SUBJ 000), title, days (MTWHF), time (military time range), room (BLDG 000)
fields = [0, 1, 3, 7, 8, 9]
course_list = [[row[i] for i in fields] for row in data_list[1:]]
sys.stdout.write(" Done!\n")

classes = []

sys.stdout.write("\n")
next_class = input("Enter a class or 0 when you're done: ").upper()
if next_class == "0":
    exit()

while next_class != "0":
    classes.append(next_class)
    next_class = input("Enter a class or 0 when you're done: ").upper()

class_options = [[x for x in course_list if x[1] == n] for n in classes]
class_choices = []

for o in class_options:
    sys.stdout.write("\nTimes for {}:\n".format(o[0][1]))
    if len(o) == 0:
        sys.stdout.write("No times found.\n")
        continue
    o.sort(key=lambda o: int(o[4].split('-')[0]))
    for t in o:
        sys.stdout.write("{:<3} @ {}".format(t[3], t[4]).ljust(18))
    sys.stdout.write("\n\n")

for c in class_options:
    for i in range(len(c)):
        sys.stdout.write(str(i) + ": {} - {} {}\n".format(c[i][1], c[i][3], c[i][4]))
    valid = False
    class_choice = int(input("Which class do you want? (number or -1 to skip): "))
    while not valid:
        if 0 <= class_choice < len(c):
            if len(class_choices) == 0:
                valid = True
                continue
            else:
                valid = not all(check_intersect(c[class_choice], x) for x in class_choices)
                if not valid:
                    sys.stdout.write("Time overlaps with another selected class!\n")
        elif class_choice == -1:
            break
        else:
            sys.stdout.write("Choice not in list!\n")
        if not valid:
            # sys.stdout.write("Invalid option! (Either not in list or time overlaps)\n")
            class_choice = int(input("Which class do you want? (number or -1 to skip): "))
    if class_choice != -1:
        class_choices.append(c[class_choice])

for s in class_choices:
    sys.stdout.write("({}) - {}: {:<3} @ {}\n".format(s[0], s[1], s[3], s[4]))

